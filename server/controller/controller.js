var CsvDb = require('../model/model');
var Excel = require('exceljs');
var workbook = new Excel.Workbook();

var odlUrl = "F://Azeem/officeData/projects/AhmerProject/CRUD_Application_Node";
const baseUrl = "http://3.142.187.60:3000"
// create and save new enrich
exports.create = (req, res) => {
    // validate request
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
        return;
    }
    // // new enrich
    const csv = new CsvDb({
        BUSINESSNAME: req.body.BUSINESSNAME,
        ADDRESS: req.body.ADDRESS,
        EIN: req.body.EIN,
        YRSINBIZ: req.body.YRSINBIZ,
        OWNERNAME: req.body.OWNERNAME,
        TEL: req.body.TEL,
        EMAIL: req.body.EMAIL,
        DOB: req.body.DOB,
        SSN: req.body.SSN,
        BANKNAME: req.body.BANKNAME,
        ACCOUNTNUMBER: req.body.ACCOUNTNUMBER
    })

    // save enrich in the database
    csv.save(csv)
        .then(data => {
            //res.send(data)
            res.redirect('/add-enrich');
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating a create operation"
            });
        });

}

// retrieve and return all enrichs/ retrive and return a single enrich
exports.find = (req, res) => {

    if (req.query.id) {
        const id = req.query.id;

        CsvDb.findById(id)
            .then(data => {
                if (!data) {
                    res.status(404).send({message: "Not found enrich with id " + id})
                } else {
                    res.send(data)
                }
            })
            .catch(err => {
                res.status(500).send({message: "Error retrieving enrich with id " + id})
            })

    } else {
        CsvDb.find()
            .then(csvs => {
                res.send(csvs)
            })
            .catch(err => {
                res.status(500).send({message: err.message || "Error Occurred while retriving enrich information"})
            })
    }


}

// Update a new idetified enrich by enrich id
exports.update = (req, res) => {
    if (!req.body) {
        return res
            .status(400)
            .send({message: "Data to update can not be empty"})
    }

    const id = req.params.id;
    CsvDb.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
        .then(data => {
            if (!data) {
                res.status(404).send({message: `Cannot Update enrich with ${id}. Maybe enrich not found!`})
            } else {
                res.send(data)
            }
        })
        .catch(err => {
            res.status(500).send({message: "Error Update enrich information"})
        })
}

// Delete a enrich with specified enrich id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    CsvDb.findByIdAndDelete(id)
        .then(data => {
            if (!data) {
                res.status(404).send({message: `Cannot Delete with id ${id}. Maybe id is wrong`})
            } else {
                res.send({
                    message: "Enrich was deleted successfully!"
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Enrich with id=" + id
            });
        });
}


const records = [];
var count = 0;
// create and import csv file new enrich
// exports.import = (req, res) => {
//     workbook.xlsx.readFile('sample1.xlsx')
//         .then(function () {
//             var worksheet = workbook.getWorksheet('Sheet1');
//             worksheet.eachRow({includeEmpty: false}, function (row, rowNumber) {
//                 if (rowNumber > 1) {
//                     const data = {
//                         BUSINESSNAME: row.values[1] ? row.values[1] : null,
//                         ADDRESS: row.values[2] ? row.values[2] : null,
//                         YRSINBIZ: row.values[3] ? row.values[3] : null,
//                         EIN: row.values[4] ? row.values[4] : null,
//                         OWNERNAME: row.values[5] ? row.values[5] : null,
//                         TEL: row.values[6] ? row.values[6] : null,
//                         EMAIL: row.values[7] ? row.values[7].text : null,
//                         DOB: row.values[8] ? row.values[8] : null,
//                         SSN: row.values[9] ? row.values[9] : null,
//                         BANKNAME: row.values[10] ? row.values[10] : null,
//                         ACCOUNTNUMBER: row.values[11] ? row.values[11] : null,
//                     }
//                     records.push(data)
//                 }
//             });
//             if (records.length > 0) {
//                 for (var i = 0; i < records.length; i++) {
//                     count++;
//                     // // new enrich
//                     const csv = new CsvDb({
//                         BUSINESSNAME: records[i].BUSINESSNAME,
//                         ADDRESS: records[i].ADDRESS,
//                         EIN: records[i].EIN,
//                         YRSINBIZ: records[i].YRSINBIZ,
//                         OWNERNAME: records[i].OWNERNAME,
//                         TEL: records[i].TEL,
//                         EMAIL: records[i].EMAIL,
//                         DOB: records[i].DOB,
//                         SSN: records[i].SSN,
//                         BANKNAME: records[i].BANKNAME,
//                         ACCOUNTNUMBER: records[i].ACCOUNTNUMBER
//                     })
//
//                     // save enrich in the database
//                     csv.save(csv)
//                         .then(data => {
//                             //res.send(data)
//                             // res.redirect('/add-enrich');
//                             if (count === records.length) {
//                                 res.redirect('/')
//                             }
//                         })
//                         .catch(err => {
//                             res.status(500).send({
//                                 message: err.message || "Some error occurred while creating a create operation"
//                             });
//                         });
//
//                 }
//             }
//         });
//
// }
//

const readXlsxFile = require("read-excel-file/node");

exports.import = async (req, res) => {
    try {
        if (req.file == undefined) {
            return res.status(400).send("Please upload an excel file!");
        }

        let path =
            __basedir  + "/assets/uploads/" + req.file.filename;

        readXlsxFile(path).then((rows) => {
            // skip header
            rows.shift();

            let records = [];

            rows.forEach((row) => {
                let record = {
                    BUSINESSNAME: row[0] ? row[0] : null,
                    ADDRESS: row[1] ? row[1] : null,
                    YRSINBIZ: row[2] ? row[2] : null,
                    EIN: row[3] ? row[3] : null,
                    OWNERNAME: row[4] ? row[4] : null,
                    TEL: row[5] ? row[5] : null,
                    EMAIL: row[6] ? row[6] : null,
                    DOB: row[7] ? row[7] : null,
                    SSN: row[8] ? row[8] : null,
                    BANKNAME: row[9] ? row[9] : null,
                    ACCOUNTNUMBER: row[10] ? row[10] : null,
                };

                records.push(record);
            });


            if (records.length > 0) {
                for (var i = 0; i < records.length; i++) {
                    count++;
                    // // new enrich
                    const csv = new CsvDb({
                        BUSINESSNAME: records[i].BUSINESSNAME,
                        ADDRESS: records[i].ADDRESS,
                        EIN: records[i].EIN,
                        YRSINBIZ: records[i].YRSINBIZ,
                        OWNERNAME: records[i].OWNERNAME,
                        TEL: records[i].TEL,
                        EMAIL: records[i].EMAIL,
                        DOB: records[i].DOB,
                        SSN: records[i].SSN,
                        BANKNAME: records[i].BANKNAME,
                        ACCOUNTNUMBER: records[i].ACCOUNTNUMBER
                    })

                    // save enrich in the database
                    csv.save(csv)
                        .then(data => {
                            //res.send(data)
                            // res.redirect('/add-enrich');
                            if (count === records.length) {
                                res.redirect('/')
                            }
                        })
                        .catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating a create operation"
                            });
                        });

                }
            }
            // CsvDb.bulkCreate(records)
            //     .then(() => {
            //         res.status(200).send({
            //             message: "Uploaded the file successfully: " + req.file.originalname,
            //         });
            //     })
            //     .catch((error) => {
            //         res.status(500).send({
            //             message: "Fail to import data into database!",
            //             error: error.message,
            //         });
            //     });
        });
    } catch (error) {
        console.log(error);
        res.status(500).send({
            message: "Could not upload the file: " + req.file.originalname,
        });
    }
};
