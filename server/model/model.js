const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    // name : {
    //     type : String,
    //     required: true
    // },
    // email : {
    //     type: String,
    //     required: true,
    //     unique: true
    // },
    BUSINESSNAME : String,
    ADDRESS : String,
    YRSINBIZ : String,
    EIN : String,
    OWNERNAME : String,
    TEL : String,
    EMAIL : String,
    DOB : String,
    SSN : String,
    BANKNAME : String,
    ACCOUNTNUMBER : String,
})

const CsvDb = mongoose.model('csv', schema);

module.exports = CsvDb;
