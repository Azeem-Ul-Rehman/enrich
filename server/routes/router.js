const express = require('express');
const route = express.Router()

const services = require('../services/render');
const controller = require('../controller/controller');
const upload = require("../middlewares/upload");

/**
 *  @description Root Route
 *  @method GET /
 */
route.get('/', services.homeRoutes);

/**
 *  @description add enrichs
 *  @method GET /add-enrich
 */
route.get('/add-enrich', services.add_enrich)

/**
 *  @description for update enrich
 *  @method GET /update-enrich
 */
route.get('/update-enrich', services.update_enrich)


// API
route.post('/api/enrichs', controller.create);
route.get('/api/enrichs', controller.find);
route.put('/api/enrichs/:id', controller.update);
route.delete('/api/enrichs/:id', controller.delete);

// route.post('/api/enrichs/import', controller.import);
route.post("/api/enrichs/import", upload.single("file"), controller.import);


module.exports = route
