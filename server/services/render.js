const axios = require('axios');

const baseUrl = "http://localhost:3000"
exports.homeRoutes = (req, res) => {
    // Make a get request to /api/enrichs
    axios.get(baseUrl + '/api/enrichs')
        .then(function (response) {

            res.render('index', {csvs: response.data});
        })
        .catch(err => {
            res.send(err);
        })


}

exports.add_enrich = (req, res) => {
    res.render('add_enrich');
}

exports.update_enrich = (req, res) => {
    axios.get(baseUrl + '/api/enrichs', {params: {id: req.query.id}})
        .then(function (response) {
            res.render("update_enrich", {enrich: response.data})
        })
        .catch(err => {
            res.send(err);
        })
}
